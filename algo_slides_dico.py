import numpy as np
import matplotlib.pyplot as plt
from hexalattice.hexalattice import *

beta=0.8   #(1.8,510-4)
gamma=0.0005  #Valeurs fonctionnelles: 0.005, 

BLANC=[1,1,1]
GRIS=[0.3,0.3,0.3]
NOIR=[0,0,0]
current_color=BLANC
bg_color=NOIR

h,l=100,100
itération =500

def voisin(i,j):
    '''Renvoit la liste des cléfs voisines au point i,j, le point lui-même n'étant pas 
        inclus.
        Les clés hors champ (pb de Bord) ne sont pas pris en compte.
    '''
    L=[]
    g=[(i,j-1),(i+1,j-1),(i+1,j),(i,j+1),(i-1,j+1),(i-1,j)]
    for (a,b) in g:
        if not(a==-1 or b==-1 or a==h or b==l):
            L.append(Clés[a][b])
    return(L)

def actualiser(c):
    if Flocon[c]["etat"]==0:
        Flocon[c]["u°"]=0
        Flocon[c]["v°"]=Flocon[c]["s"]
        Flocon[c]["v'"]=Flocon[c]["v°"]+gamma
    else:
        Flocon[c]["u°"]=Flocon[c]["s"]
        Flocon[c]["v°"]=0

print("Initialisation en cours .... ")
Clés=[["C_%i_%i"%(i,j) for j in range(l)] for i in range(h)]
Flocon={}
for i in range(h):
    print("i= "+str(i))
    for j in range(l):
        Flocon[Clés[i][j]]={"pos":[i,j],"etat":1,'s':beta,"u'":0,"v'":0,"voisins":voisin(i,j),"couleur":bg_color}
        Flocon[Clés[i][j]]["u°"]=Flocon[Clés[i][j]]["s"]

x0=int(l/2)
y0=int(h/2)
Flocon[Clés[x0][y0]]["etat"]=0
Flocon[Clés[x0][y0]]['s']=1
for x in Flocon[Clés[x0][y0]]["voisins"]:
    Flocon[x]["etat"]=0
print("done \n")


for c in Flocon.keys():
    actualiser(c)


#print("Itération N°1/"+str(itération))
for i in range(1,itération+1):
    #if np.floor(i/100)!=np.floor((i-1)/100):
    print("Itération N°"+str(i)+"/"+str(itération))
    for c in Flocon.keys():
        Flocon[c]["u'"]=1/2*Flocon[c]["u°"]
        for v in Flocon[c]["voisins"]:
            Flocon[c]["u'"]+=1/12*Flocon[v]["u°"]
    for c in Flocon.keys():
        Flocon[c]["s"]=Flocon[c]["v'"]+Flocon[c]["u'"]
        if Flocon[c]["s"]>=1:
            Flocon[c]["etat"]=0
            Flocon[c]["couleur"]=current_color
            for x in Flocon[c]["voisins"]:
                Flocon[x]["etat"]=0
        actualiser(c)



def affichage(angle,h,l,mil_i,mil_j):
    print("Affichage en cours ... ")
    plt.figure()
    for c in Flocon.keys():
        if Flocon[c]["s"]>=1:
            Flocon[c]["couleur"]=current_color

    rotation=np.array([[np.cos(angle),-np.sin(angle)],[np.sin(angle),np.cos(angle)]])
    coloration=np.zeros((h,l,3))
    for c in Flocon.keys():
        pos=np.array([Flocon[c]["pos"][0]-mil_i,Flocon[c]["pos"][1]-mil_j])
        coord=np.dot(rotation,pos)
        x=int(coord[0]*np.sqrt(1/3)+mil_i)
        y=int((coord[1]))+mil_j
        if y <h:
            if not((coloration[x][y]==current_color).all()):
                for i in range(3):
                    coloration[x][y][i]=Flocon[c]["couleur"][i]
    plt.title(r"$\beta$= "+str(beta)+r", $\gamma$="+str(gamma))
    plt.imshow(coloration, norm=None)
    plt.show()

# affichage(np.pi/4,h,l,x0,y0)

def affichage_hexa(h,l,x0,y0):
    print("Affichage en cours ... ")
    colors=np.zeros((h*l,3))
    hex_centers, _ = create_hex_grid(nx=h,
                                 ny=l,
                                 do_plot=False)
    x_hex_coords = hex_centers[:, 0]
    y_hex_coords = hex_centers[:, 1]
    for c in Flocon.keys():
        (i,j)=Flocon[c]["pos"]
        n=i*l+j
        colors[n][0]=Flocon[c]["couleur"][0]
        colors[n][1]=Flocon[c]["couleur"][1]
        colors[n][2]=Flocon[c]["couleur"][2]
    
    plot_single_lattice_custom_colors(x_hex_coords, y_hex_coords,
                                      face_color=colors,
                                      edge_color=colors,
                                      min_diam=1,
                                      plotting_gap=0.0,
                                      rotate_deg=0)
    plt.title(r"$\beta$= "+str(beta)+r", $\gamma$="+str(gamma))
    affichage(np.pi/4,h,l,x0,y0)

affichage_hexa(h,l,x0,y0)