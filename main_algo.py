import random as rd
import numpy as np
import matplotlib.pyplot as plt
import pprint as pp
from hexalattice.hexalattice import *

l,h=100,100  # l est la largeur du flocon, h sa hauteur
itération=500  #nb itération de l'algorythme d'actualisation
BLANC=[1,1,1]
NOIR=[0,0,0]
VERT=[0,1,0]
GRIS=[0.3,0.3,0.3]
current_color=BLANC


#paramètres du problème
rho=0.35       #usual 0.65
beta=1.4       #usual 1.09
k=0.05       #usual 0.0001  -- referenced in paper as kappa
mu=0.15         #usual 0.14
gamma=0.01   #usual 0.00001
alpha=0.001      #usual 0.1
teta=0.015   #usual 0.0745
sigma=0.00005   #usual 0.00002



def voisin(i,j):
    '''Renvoit la liste des cléfs voisines au point i,j, le point lui-même n'étant pas 
        inclus.
        Les clés hors champ (pb de Bord) ne sont pas pris en compte.
    '''
    L=[]
    g=[(i,j-1),(i+1,j-1),(i+1,j),(i,j+1),(i-1,j+1),(i-1,j)]
    for (a,b) in g:
        if not(a==-1 or b==-1 or a==h or b==l):
            L.append(Clés[a][b])
    return(L)

def actualiser():
    '''Remplace toues les valeurs ° par les valeurs ' à la fin de chaque étape
        pour la réutilisation future.
    '''
    for c in Flocon.keys():
        Flocon[c]["a°"]=Flocon[c]["a'"]
        Flocon[c]["b°"]=Flocon[c]["b'"]
        Flocon[c]["c°"]=Flocon[c]["c'"]
        Flocon[c]["d°"]=Flocon[c]["d'"]


#Initialisation du dictionnaire Flocon de l'état des graphes 
Flocon = {}         #sous la forme Flocon={Clés:{coordonnées,a(t),b(t),c(t),d(t),couleur pour tracer,[liste des voisins], etat actuel}}
Clés=[["C_%i_%i"%(i,j) for j in range(l)] for i in range(h)]

for i in range(h):
    print("i= "+str(i))
    for j in range(l):
        Flocon[Clés[i][j]]={"pos":[i,j],"a'":0 , "b'":0 , "c'":0 , "d'":rho,"a°":0 , "b°":0 , "c°":0 , "d°":rho , "couleur":GRIS, "voisins":voisin(i,j),"etat":3}


#gel de la première particule
mil_i,mil_j= int(h/2), int(l/2)
Flocon[Clés[mil_i][mil_j]]={"pos":[mil_i,mil_j],"a'":1 , "b'":0 , "c'":1 , "d'":0 , "a°":1 , "b°":0 , "c°":1 , "d°":0 ,"couleur":current_color, "voisins":voisin(mil_i,mil_j),"etat":1}
for c in Flocon[Clés[mil_i][mil_j]]["voisins"]:
    Flocon[c]["etat"]=2


#boucle principale
for v in range(itération):
    print("Itération N°"+str(v)+"/"+str(itération))
    #i) Diffusion
    for c in Flocon.keys():
        if Flocon[c]["etat"]==3:
            S=Flocon[c]["d°"]
            for x in Flocon[c]["voisins"]:
                S += Flocon[x]["d°"]
            S *= (1/7)
            
        if Flocon[c]["etat"]==2:
            S=Flocon[c]["d°"]
            for x in Flocon[c]["voisins"]:
                if Flocon[x]["etat"]==1:
                    S+=Flocon[c]["d°"]
                else:    
                    S += Flocon[x]["d°"]
            S *= (1/7)
        Flocon[c]["d'"]  = S  
    

    actualiser()

    #ii) Freezing
    for c in Flocon.keys():
        if Flocon[c]["etat"]==2:
            Flocon[c]["b'"] = Flocon[c]["b°"] + (1-k)*Flocon[c]["d°"]
            Flocon[c]["c'"] = Flocon[c]["c°"] + k*Flocon[c]["d°"]
            Flocon[c]["d'"] = 0


    actualiser()
    
    #iii) Attachement
    new_attached=[]
    for c in Flocon.keys():
        if Flocon[c]["etat"]==2:
            n = 0
            for x in Flocon[c]["voisins"]:
                n += Flocon[x]["a°"]
            
            if (n==1 or n==2) and Flocon[c]["b°"] >= beta:
                #gel de la particule
                Flocon[c]["a'"] = 1
                Flocon[c]["c'"] = Flocon[c]["b°"] + Flocon[c]["c°"] 
                Flocon[c]["b'"] = 0
                Flocon[c]["couleur"] = current_color
                Flocon[c]["etat"]=1
                new_attached.append(c)
                
            if n==3 :
                if Flocon[c]["b°"] >= 1:
                    #gel de la particule
                    Flocon[c]["a'"] = 1
                    Flocon[c]["c'"] = Flocon[c]["b°"] + Flocon[c]["c°"] 
                    Flocon[c]["b'"] = 0
                    Flocon[c]["couleur"] =current_color
                    Flocon[c]["etat"]=1
                    new_attached.append(c)

                elif Flocon[c]["b°"] >= alpha:
                    S = Flocon[c]["d°"]
                    for x in Flocon[c]["voisins"]:
                        S += Flocon[c]["d°"]
                    
                    if S < teta :
                        #gel de la particule
                        Flocon[c]["a'"] = 1
                        Flocon[c]["c'"] = Flocon[c]["b°"] + Flocon[c]["c°"] 
                        Flocon[c]["b'"] = 0
                        Flocon[c]["couleur"] = current_color
                        Flocon[c]["etat"]=1
                        new_attached.append(c)
            
            if n==4 :
                #gel de la particule
                Flocon[c]["a'"] = 1
                Flocon[c]["c'"] = Flocon[c]["b°"] + Flocon[c]["c°"] 
                Flocon[c]["b'"] = 0
                Flocon[c]["couleur"] = current_color
                Flocon[c]["etat"]=1
                new_attached.append(c)

    #update de l'état des voisins des nouvelles particules gelées
    for c in new_attached:
        for x in Flocon[c]["voisins"]:
                    if Flocon[x]["etat"]==3:
                        Flocon[x]["etat"]=2            
    
    
    actualiser()
    
    #iv) Melting
    for c in Flocon.keys():
        if Flocon[c]["etat"]==2:
            Flocon[c]["b'"] = (1-mu)*Flocon[c]["b°"]
            Flocon[c]["c'"] = (1-gamma)*Flocon[c]["c°"]
            Flocon[c]["d'"] = Flocon[c]["d°"] + mu*Flocon[c]["b°"] + gamma*Flocon[c]["c°"]
    
    
    actualiser()
    
    #v) Noise
    for c in Flocon.keys():
        if rd.random() < 0.5 :
            Flocon[c]["d'"] = (1+sigma)*Flocon[c]["d°"]
        else:
            Flocon[c]["d'"] = (1-sigma)*Flocon[c]["d°"]
    actualiser()
            
#pp.pprint(Flocon) 
  
def affichage(angle,h,l,mil_i,mil_j):
    rotation=np.array([[np.cos(angle),-np.sin(angle)],[np.sin(angle),np.cos(angle)]])
    coloration=np.zeros((h,l,3))
    for c in Flocon.keys():
        pos=np.array([Flocon[c]["pos"][0]-mil_i,Flocon[c]["pos"][1]-mil_j])
        coord=np.dot(rotation,pos)
        x=int(coord[0]+mil_i)
        y=int((coord[1])*np.sqrt(1/3))+mil_j
        if x <l:
            if not((coloration[x][y]==current_color).all()):
                for i in range(3):
                    coloration[x][y][i]=Flocon[c]["couleur"][i]
    plt.imshow(coloration, norm=None)
    plt.show()

#affichage(np.pi/4,h,l,mil_i,mil_j)

def affichage_hexa(h,l):
    colors=np.zeros((h*l,3))
    hex_centers, _ = create_hex_grid(nx=h,
                                 ny=l,
                                 do_plot=False)
    x_hex_coords = hex_centers[:, 0]
    y_hex_coords = hex_centers[:, 1]
    for c in Flocon.keys():
        (i,j)=Flocon[c]["pos"]
        n=i*l+j
        colors[n][0]=Flocon[c]["couleur"][0]
        colors[n][1]=Flocon[c]["couleur"][1]
        colors[n][2]=Flocon[c]["couleur"][2]
    
    plot_single_lattice_custom_colors(x_hex_coords, y_hex_coords,
                                      face_color=colors,
                                      edge_color=colors,
                                      min_diam=1,
                                      plotting_gap=0.0,
                                      rotate_deg=0)
    plt.show()

affichage_hexa(h,l)